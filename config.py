class Config(object):

    yolo_config = "cfg/yolov4-obj.cfg"
    yolo_classes = "data/yolov4-obj.names"
    yolo_weights = "backup/yolov4-obj_best.weights"
    conf_threshold = 0.5
    nms_threshold = 0.4
    workers = 1
    batch_size = 4
    model_gan = 'models_gan/RRDB_PSNR_x4.pth'