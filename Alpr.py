import time
from typing import List
import cv2
import argparse
import easyocr
from matplotlib import pyplot as plt
import numpy as np
from config import Config
from tqdm import tqdm
import logging
import RRDBNet_arch as arch
import torch
logger = logging.getLogger(__name__)
CHARS = [ 
         '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
         'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'K',
         'L', 'M', 'N', 'P', 'S', 'T', 'U', 'V',
         'X', 'Y', 'Z'
         ]


def get_output_layers(net):
    layer_names = net.getLayerNames()
    output_layers = [layer_names[i[0] - 1]
                     for i in net.getUnconnectedOutLayers()]

    return output_layers


def gofoward(blob, net):
    net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
    net.setPreferableTarget(cv2.dnn.DNN_TARGET_CUDA)
    net.setInput(blob)
    outs = net.forward(get_output_layers(net))
    return outs


class PlateDetector:
    def __init__(self, scale=0.00392):
        logger.info("Start")
        self.config = Config.yolo_config
        self.weights = Config.yolo_weights
        self.scale = scale
        self.net = cv2.dnn.readNet(self.weights, self.config)
        self.conf_threshold = Config.conf_threshold
        self.nms_threshold = Config.nms_threshold
        self.reader = easyocr.Reader(['en'],gpu=True, model_storage_directory="models")
        logger.info("Finish itialization")
        self.gan_model = arch.RRDBNet(3, 3, 64, 23, gc=32)
        self.gan_model.load_state_dict(torch.load(Config.model_gan), strict=True)
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.gan_model.eval()
        self.gan_model = self.gan_model.to(self.device)
    
    def blob_image(self, image):
        logger.info(f"Loading from {image}")
        image = cv2.imread(image)
        Width = image.shape[1]
        Height = image.shape[0]
        blob = cv2.dnn.blobFromImage(
            image, self.scale, (416, 416), (0, 0, 0), True, crop=False)
        logger.debug("Finish loading")
        return blob, Width, Height

    def detection(self, image):
        class_ids = []
        confidences = []
        boxes = []
        blob, Width, Height = self.blob_image(image)
        outs = gofoward(blob,self.net)
        for out in outs:
            for detection in out:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]
                if confidence > 0.5:
                    center_x = int(detection[0] * Width)
                    center_y = int(detection[1] * Height)
                    w = int(detection[2] * Width)
                    h = int(detection[3] * Height)
                    x = center_x - w / 2
                    y = center_y - h / 2
                    class_ids.append(class_id)
                    confidences.append(float(confidence))
                    boxes.append([x, y, w, h])
        indices = cv2.dnn.NMSBoxes(
            boxes, confidences, self.conf_threshold, self.nms_threshold)
        return indices, boxes

    def crop_image(self, image):
        img = cv2.imread(image)
        indices, boxes = self.detection(image)
        crop_imgs = []
        for i in indices:
            i = i[0]
            box = boxes[i]
            x = int(box[0])
            y = int(box[1])
            w = int(box[2])
            h = int(box[3])
            
            crop_img = img[y:y+h, x:x+w]
            crop_imgs.append(crop_img)
        return crop_imgs

    def recognition(self, crop_imgs):

        result = self.reader.readtext(
            crop_imgs, text_threshold=0.5, min_size=20,allowlist=CHARS,contrast_ths=0.5, mag_ratio=1.5, batch_size=Config.batch_size, workers=0)

        return result

    def upscale(self,img:np.array):
        img = img * 1.0 / 255
        img = torch.from_numpy(np.transpose(img[:, :, [2, 1, 0]], (2, 0, 1))).float()
        img_LR = img.unsqueeze(0)
        img_LR = img_LR.to(self.device)
        with torch.no_grad():
            output = self.gan_model(img_LR).data.squeeze().float().cpu().clamp_(0, 1).numpy()
        output = np.transpose(output[[2, 1, 0], :, :], (1, 2, 0))
        output = (output * 255.0).round()
        return output
    
    def Pipeline(self, image, Upscale :bool= False):
        crop = self.crop_image(image)
        # print(crop)
        # crop = upscale(crop)
        nums = []
        for cop in crop:
            if Upscale:
                cop = self.upscale(cop)
                num = self.recognition(cop)
                nums.append(num)
            return nums


system = PlateDetector()
path ='test.jpg'
# img = cv2.imread(path)
num = system.recognition(path)
print(num)